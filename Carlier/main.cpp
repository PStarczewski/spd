#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include "../RPQ/common.hpp"

int fallback_counter = 0;

struct singleEntry {
	int r;
	int p;
	int q;
	int index;

	singleEntry(): r(0), p(0), q(0), index(0) {};
	singleEntry(std::vector<int> vec, int index) : r(vec[0]), p(vec[1]), q(vec[2]), index(index) {}

	void print() {
		std::cout << r << " " << p << " " << q << " " << std::endl;
	}
};

int sortSchragePmtn(std::vector<singleEntry>* unorderedTasks) {
	int t = 0;
	int Cmax = 0;
	singleEntry l;
	int q0 = 0;
	std::vector<singleEntry>* partiallyOrderedTasks = new std::vector<singleEntry>; //Ng
	std::vector<singleEntry>* fullyOrderedTasks = new std::vector<singleEntry>;

	while (partiallyOrderedTasks->size() != 0 || unorderedTasks->size() != 0) {
		while (unorderedTasks->size() != 0 && RPQ::findMinR(unorderedTasks) <= t) {

			int j = 0;
			for (j; j < unorderedTasks->size(); ++j) {
				if ((*unorderedTasks)[j].r == RPQ::findMinR(unorderedTasks)) {
					break;
				}
			}

			partiallyOrderedTasks->push_back((*unorderedTasks)[j]);

			if ((*unorderedTasks)[j].q > l.q) {
				l.p = t - (*unorderedTasks)[j].r;
				t = (*unorderedTasks)[j].r;

				if (l.p > 0) {
					partiallyOrderedTasks->push_back((*unorderedTasks)[j]);
				}
			}

			std::vector<singleEntry>::iterator it = unorderedTasks->begin();
			std::advance(it, j);
			unorderedTasks->erase(it);

			
		}

		if (partiallyOrderedTasks->size() == 0) {
			t = RPQ::findMinR(unorderedTasks);
		}
		else {
			int j = 0;

			for (j; j < partiallyOrderedTasks->size(); ++j) {
				if ((*partiallyOrderedTasks)[j].q == RPQ::findMaxQ(partiallyOrderedTasks)) {
					break;
				}
			}

			l = (*partiallyOrderedTasks)[j];
			t = t + (*partiallyOrderedTasks)[j].p;
			Cmax = std::max(Cmax, t + (*partiallyOrderedTasks)[j].q) + 3;

			std::vector<singleEntry>::iterator it = partiallyOrderedTasks->begin();
			std::advance(it, j);
			partiallyOrderedTasks->erase(it);
		}
	}

	return Cmax;
}

std::vector<singleEntry>* sortSchrage(std::vector<singleEntry>* unorderedTasks) {
	int t = RPQ::findMinR(unorderedTasks);

	//Empty on start!
	std::vector<singleEntry>* partiallyOrderedTasks = new std::vector<singleEntry>;
	std::vector<singleEntry>* fullyOrderedTasks = new std::vector<singleEntry>;

	while (partiallyOrderedTasks->size() != 0 || unorderedTasks->size() != 0) {
		while (unorderedTasks->size() != 0 && RPQ::findMinR(unorderedTasks) <= t) {

			int j = 0;
			for (j; j < unorderedTasks->size(); ++j) {
				if ((*unorderedTasks)[j].r == RPQ::findMinR(unorderedTasks)) {
					break;
				}
			}

			partiallyOrderedTasks->push_back((*unorderedTasks)[j]);

			std::vector<singleEntry>::iterator it = unorderedTasks->begin();
			std::advance(it, j);
			unorderedTasks->erase(it);
		}

		if (partiallyOrderedTasks->size() == 0) {
			t = RPQ::findMinR(unorderedTasks);
		}
		else {
			int j = 0;

			for (j; j < partiallyOrderedTasks->size(); ++j) {
				if ((*partiallyOrderedTasks)[j].q == RPQ::findMaxQ(partiallyOrderedTasks)) {
					break;
				}
			}

			t = t + (*partiallyOrderedTasks)[j].p;
			fullyOrderedTasks->push_back((*partiallyOrderedTasks)[j]);

			std::vector<singleEntry>::iterator it = partiallyOrderedTasks->begin();
			std::advance(it, j);
			partiallyOrderedTasks->erase(it);
		}
	}

	return fullyOrderedTasks;
}

void sortCarlier(std::vector<singleEntry>* unorderedTasks, int UB = 999999, int dataNum = 0) {
	std::vector<singleEntry> temp = *unorderedTasks;
	std::vector<singleEntry>* schrageVec = sortSchrage(unorderedTasks);
	std::vector<singleEntry>* newVec = nullptr;
	int U = RPQ::calculateCMaxFromVector(schrageVec);

	if (UB >= U) {
		UB = U;
		newVec = schrageVec;
	}
	else {
		newVec = &temp;
	}

	std::vector<int> ABC = Carlier::calculateCBA(newVec);

	if (ABC[2] == 0 || fallback_counter > 10) {
		RPQ::printDataset(dataNum, newVec);
		return;
	}



	int primR = (*newVec)[ABC[2] + 1].r;
	int primP = 0;
	int primQ = (*newVec)[ABC[2] + 1].q;

	for (int i = ABC[2] + 1; i < ABC[1] + 1; ++i) {
		if ((*newVec)[i].r < primR) {
			primR = (*newVec)[i].r;
		}
		primP += (*newVec)[i].p;
		if ((*newVec)[i].q < primQ) {
			primQ = (*newVec)[i].q;
		}
	}

	int backupR = (*newVec)[ABC[2]].r;
	(*newVec)[ABC[2]].r = std::max((*newVec)[ABC[2]].r, primR + primP);

  std::vector<singleEntry> temp2 = *newVec;
	int LB = sortSchragePmtn(&temp2);

	if (LB < U) {
		fallback_counter++;
		sortCarlier(newVec, U, dataNum);
		//return nullptr;
	}
	else {
		(*newVec)[ABC[2]].r = backupR;

		int backupQ = (*newVec)[ABC[2]].q;
		(*newVec)[ABC[2]].q = std::max((*newVec)[ABC[2]].q, primQ + primP);

		std::vector<singleEntry> temp3 = *newVec;
		LB = sortSchragePmtn(&temp3);

		if (LB < U) {
			fallback_counter++;
			sortCarlier(newVec, U, dataNum);
			//return nullptr;
		}
		else {
			(*newVec)[ABC[2]].q = backupQ;
			
			RPQ::printDataset(dataNum, newVec);
			return;
		}
	}
	return;
}

int main() {
	for (int i = 0; i < 9; ++i) {
		fallback_counter = 0;
		std::string name = "data" + std::to_string(i) + ".txt";
		std::vector<singleEntry>* vec = loadDataToVector<singleEntry>(name, 3);
		sortCarlier(vec, 999999, i);
	}
	
}