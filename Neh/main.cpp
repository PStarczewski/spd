#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include <chrono>
#include "../RPQ/common.hpp"

struct singleEntry {
	std::vector<int> timeCollection;
	int index;
	int Cmax;

	singleEntry() {}
	singleEntry(std::vector<int> vec, int index) : index(index) {
		timeCollection = vec;
	}

	void print() {
		for(int i = 0; i < timeCollection.size(); ++i)
		std::cout << timeCollection[i] << " "; 
	}
};

std::vector<singleEntry> insertEntry(std::vector<singleEntry> newSeq, int index, singleEntry entry) {
	newSeq.insert(newSeq.begin() + index, entry);

	return newSeq;
}

int makeSpan(std::vector<singleEntry> tempSeq, std::vector<singleEntry> data) {
	std::vector<std::vector<int>> c_ij;
	c_ij.resize(data.size() + 1);

	for (int i = 0; i < data.size() + 1; i++) {
		c_ij[i].resize(data[0].timeCollection.size() + 1);
		c_ij[i][0] = 0;
	}

	for (int i = 0; i < data[0].timeCollection.size(); i++) {
		c_ij[0][i] = 0;
	}
		

	int currentSumTimeCMax{};

	for (int i = 1; i <= tempSeq.size(); i++) {
		for (int j = 1; j <= data[0].timeCollection.size(); j++) {
			currentSumTimeCMax = tempSeq[i - 1].timeCollection[j - 1];

			c_ij[i][j] = std::max(c_ij[i - 1][j], c_ij[i][j - 1]) + currentSumTimeCMax;
		}
	}

	return c_ij[tempSeq.size()][data[0].timeCollection.size()];
}

std::vector<singleEntry> sortNeh(std::vector<singleEntry> data) {
	//STEP ONE, SUM
	std::vector<std::pair<int,int>> Ti;


	for (int i = 0; i < data.size(); ++i) {
		int T = 0;
		for (int j = 0; j < data[i].timeCollection.size(); ++j) {
			T += data[i].timeCollection[j];
		}
		Ti.push_back(std::pair<int, int>(data[i].index, T));
	}

	//STEP TWO, SORT
	sort(Ti.begin(), Ti.end(), [=](std::pair<int, int> a, std::pair<int, int> b)
	{
		return a.second < b.second;
	});

	for (int i = 0; i < data.size(); ++i) {
		if (Ti[i].first != (data[i].index)) {
			std::swap(data[i], data[Ti[i].first - 1]);
		}
	}
	

	//STEP THREE, PARTIAL SEQ
	std::vector<singleEntry> newSeq;
	std::vector<singleEntry> bestSeq;
	newSeq.push_back(data[0]);
	int finalCmax = 0;

	for (int i = 1; i < data.size(); ++i) {
		int minCmax = 99999999;

		for (int j = 0; j < i + 1; ++j) {
			std::vector<singleEntry> tempSeq = insertEntry(newSeq, j, data[i]);
			int Cmax_temp = makeSpan(tempSeq, data);
			if (minCmax > Cmax_temp) {
				bestSeq = tempSeq;
				minCmax = Cmax_temp;
			}
		}
		newSeq = bestSeq;
		finalCmax = minCmax;
	}

	newSeq[0].Cmax = finalCmax;
	return newSeq;
}

int main() {
	std::vector<std::vector<singleEntry>>* vec = Neh::loadDataToVector<singleEntry>();

	for (int i = 120; i < 121; ++i) {
		auto start = std::chrono::steady_clock::now();
		std::vector<singleEntry> vec2 = sortNeh((*vec)[i]);
		auto end = std::chrono::steady_clock::now();

		std::string order;
		for (int j = 0; j < vec2.size(); ++j) {
			order += std::to_string(vec2[j].index);
			order += " ";
		}
		std::cout << "data" << i << ": Cmax: " << vec2[0].Cmax << ", Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << "ms, Order: " << order << std::endl;
	}
	
}