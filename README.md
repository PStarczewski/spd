# Labolatorium SPD, W4, AiR, ART 2019/2020, PT 13:15 - 15:00

### Autor: Paweł Starczewski

Lista ćwiczeń wraz z odnośnikami do poszczególnych projektów. Każdy z projektów zawiera sprawozdanie wraz z informacjami o sugerowanej ocenie w postaci pliku .md:

 *  1. [RPQ](https://gitlab.com/PStarczewski/spd/-/tree/master/RPQ)         - 3.0
 *  2. [WiTi](https://gitlab.com/PStarczewski/spd/-/tree/master/WiTi)       - 3.0
 *  3. [Neh](https://gitlab.com/PStarczewski/spd/-/tree/master/Neh)         - 3.0
 *  4. [Schrage](https://gitlab.com/PStarczewski/spd/-/tree/master/Schrage) - 3.0
 *  5. [Carlier](https://gitlab.com/PStarczewski/spd/-/tree/master/Carlier) - 2.5
 ---
                                                                            SUM:  3.0