#include <string>
#include <vector>

template <typename T>
std::vector<T>* loadDataToVector(std::string fileName, int dataSize) {
	std::ifstream f(fileName);
	std::vector<T>* ret = new std::vector<T>;
	std::vector<int> data(dataSize);
	int index = 0;

	while (f) {
		for (int i = 0; i < dataSize; ++i) {
			f >> data[i];
		}
		index++;

		ret->push_back(T(data, index));
	}

	ret->pop_back();
	return ret;
}

template <typename T>
void printDataFromVector(std::vector<T>* data) {
	std::cout << data->size() << std::endl;
	for (int i = 0; i < data->size(); ++i) {
		(*data)[i].print();
	}
	std::cout << std::endl;
}

template <typename T>
void printOrderFromVector(std::vector<T>* data) {
	for (int i = 0; i < data->size(); ++i) {
		std::cout << (*data)[i].index << " ";
	}
	std::cout << "\n" << std::endl;
}

template <typename T>
void printDataset(int setNum, std::vector<T>* data) {
	std::cout << "\nDATA" << setNum << " OUTPUT:" << std::endl;
	printDataFromVector<T>(data);
	printOrderFromVector<T>(data);
	std::cout << "--------------------------------------------------------------------------" << std::endl;
}

namespace Carlier {
	template <typename T>
	std::vector<int> calculateCBA(std::vector<T>* data) {
		if (data->size() == 0) {
			return std::vector<int>{0, 0, 0};
		}
		int Cmax = RPQ::calculateCMaxFromVector(data);
		std::vector<int> workTime;
		int prep = 0;
		int c = 0;
		int b = 0;
		int a = 0;

		//PREPARE DATA
		for (int i = 0; i < data->size(); ++i) {
			prep = std::max(prep, (*data)[i].r);
			prep += (*data)[i].p;

			workTime.push_back(prep);
		}
		
		//CALC B
		for (int i = 0; i < data->size(); ++i) {
			if (Cmax == workTime[i] + (*data)[i].q) {
				b = i;
			}
		}
		

		//CALC A
		for (int i = 0; i < b + 1; ++i) {
			int sumP = 0;
			for (int j = i; j < b + 1; ++j) {
				sumP += (*data)[j].p;
			}

			if (Cmax == (*data)[i].r + sumP + (*data)[b].q) {
				a = i;
				break;
			}
		}

		//if (a == 0) {
		//	a = b;
		//}
		//if (a = 99999) {
		//	a = b;
		//}
		/*
		for (int i = b; i > -1; --i) {
			if (vecA[i] != 1) {
				a = i + 1;
				break;
			}
		}
		*/
		for (int i = a; i <= b; ++i) {
			if ((*data)[i].q < (*data)[b].q) {
				c = i;
			}
		}

		return std::vector<int>{a, b, c};
	}
}

namespace Neh {
	template <typename T>
	std::vector<std::vector<T>>* loadDataToVector() {
		std::string fileName = "data.txt";
		std::ifstream f(fileName);
		std::vector<std::vector<T>>* dataArray = new std::vector<std::vector<T>>;
		
		for (int j = 0; j < 121; ++j) {
			
			std::string name;
			int dataSize, dataLength;

			f >> name;
			f >> dataLength;
			f >> dataSize;

			std::vector<T> ret(dataLength);
			std::vector<int> data(dataSize);
			int index = 0;

			for (int i = 0; i < dataLength; ++i) {
				for (int j = 0; j < dataSize; ++j) {
					f >> data[j];
				}
				index++;

				ret[i] = (T(data, index));
			}

			//ret.pop_back();

			dataArray->push_back(ret);
		}
	
		return dataArray;
	}
}

namespace WiTi {
	template <typename T>
	int calcOPT(std::vector<T>* data) {
		int opt = 0;
		int time = 0;

		for (int i = 0; i < data->size(); ++i) {
			time += (*data)[i].time;

			if (time - (*data)[i].finishTime > 0) {
				opt += (time - (*data)[i].finishTime) * (*data)[i].weight;
			}

		}

		return opt;
	}
	
	template <typename T>
	void printDataset(int setNum, std::vector<T>* data) {
		std::cout << "\nDATA" << setNum << " OUTPUT:" << std::endl;
		printDataFromVector<T>(data);
		printOrderFromVector<T>(data);
		std::cout << "opt: " << WiTi::calcOPT(data) << "\n" << std::endl;
		std::cout << "--------------------------------------------------------------------------" << std::endl;
	}
}

namespace RPQ {
	template <typename T>
	int calculateCMaxFromVector(std::vector<T>* data) {
		int max = 0;
		int sTT = 0;

		for (int i = 0; i < data->size(); ++i) {
			sTT = std::max(sTT, (*data)[i].r);
			sTT += (*data)[i].p;
			max = std::max(max, sTT + (*data)[i].q);
		}

		return max;
	}

	template <typename T>
	int findMinR(std::vector<T>* data) {
		int min = 1000000;

		if (data->size() == 0) return 0;

		for (int i = 0; i < data->size(); ++i) {
			min = std::min((*data)[i].r, min);
		}

		return min;
	}

	template <typename T>
	int findMaxQ(std::vector<T>* data) {
		int max = 0;

		if (data->size() == 0) return 0;

		for (int i = 0; i < data->size(); ++i) {
			max = std::max((*data)[i].q, max);
		}

		return max;
	}

	template <typename T>
	void printDataset(int setNum, std::vector<T>* data) {
		std::cout << "\nDATA" << setNum << " OUTPUT:" << std::endl;
		printDataFromVector<T>(data);
		printOrderFromVector<T>(data);
		std::cout << "Cmax: " << RPQ::calculateCMaxFromVector(data) << "\n" << std::endl;
		std::cout << "--------------------------------------------------------------------------" << std::endl;
	}
}
