#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include "common.hpp"

struct singleEntry {
	int r;
	int p;
	int q;
	int index;

	singleEntry(std::vector<int> vec, int index) : r(vec[0]), p(vec[1]), q(vec[2]), index(index) {}
	
	void print() {
		std::cout << r << " " << p << " " << q << " " << std::endl;
	}
};

std::vector<singleEntry>* sortSchrage(std::vector<singleEntry>* unorderedTasks) {
	int t = RPQ::findMinR(unorderedTasks);

	//Empty on start!
	std::vector<singleEntry>* partiallyOrderedTasks = new std::vector<singleEntry>;
	std::vector<singleEntry>* fullyOrderedTasks = new std::vector<singleEntry>;

	while (partiallyOrderedTasks->size() != 0 || unorderedTasks->size() != 0) {
		while (unorderedTasks->size() != 0 && RPQ::findMinR(unorderedTasks) <= t) {

			int j = 0;
			for (j; j < unorderedTasks->size(); ++j) {
				if ((*unorderedTasks)[j].r == RPQ::findMinR(unorderedTasks)) {
					break;
				}
			}

			partiallyOrderedTasks->push_back((*unorderedTasks)[j]);

			std::vector<singleEntry>::iterator it = unorderedTasks->begin();
			std::advance(it, j);
			unorderedTasks->erase(it);
		}

		if (partiallyOrderedTasks->size() == 0) {
			t = RPQ::findMinR(unorderedTasks);
		}
		else {
			int j = 0;

			for (j; j < partiallyOrderedTasks->size(); ++j) {
				if ((*partiallyOrderedTasks)[j].q == RPQ::findMaxQ(partiallyOrderedTasks)) {
					break;
				}
			}

			t = t + (*partiallyOrderedTasks)[j].p;
			fullyOrderedTasks->push_back((*partiallyOrderedTasks)[j]);

			std::vector<singleEntry>::iterator it = partiallyOrderedTasks->begin();
			std::advance(it, j);
			partiallyOrderedTasks->erase(it);
		}
	}

	return fullyOrderedTasks;
}

int main() {
	int sumExecTime = 0;

	std::vector<singleEntry>* vec = loadDataToVector<singleEntry>("data1.txt", 3);
	std::vector<singleEntry>* vec2 = sortSchrage(vec);
	RPQ::printDataset(1, vec2);
	sumExecTime += RPQ::calculateCMaxFromVector(vec2);

	vec = loadDataToVector<singleEntry>("data2.txt", 3);
	vec2 = sortSchrage(vec);
	RPQ::printDataset(2, vec2);
	sumExecTime += RPQ::calculateCMaxFromVector(vec2);

	vec = loadDataToVector<singleEntry>("data3.txt", 3);
	vec2 = sortSchrage(vec);
	RPQ::printDataset(3, vec2);
	sumExecTime += RPQ::calculateCMaxFromVector(vec2);

	vec = loadDataToVector<singleEntry>("data4.txt", 3);
	vec2 = sortSchrage(vec);
	RPQ::printDataset(4, vec2);
	sumExecTime += RPQ::calculateCMaxFromVector(vec2);

	std::cout << "SUM: " << sumExecTime;
}