﻿# WiTi

## Informacje podstawowe

 * **Autor: Paweł Starczewski**
 * **Termin oddania: 30.05.2020**
 * **Nr ćwiczenia: 2**
 * **Sugerowana ocena: 3.0 (Ocena sugerowana jest taka ze względu na 67 dniowe spóźnienie w oddaniu pracy)**

## Informacje o ćwiczeniu

### Programowanie dynamiczne

Programowanie dynamiczne jest metodą rozwiązywania problemów optymalizacyjnych poprzez kombinację rozwiązań podproblemów. Jego autorem jest Richard Bellman. W przypadku problemu rozkładalnego na podproblemy jest ona faworyzowana, ponieważ w przypadku rozwiązywania tegóż problemu rekursywnie jego złozoność będzie wykładnicza, a co za tym idzie, bardzo czasochłonna.

Podproblemy w przypadku algorytmów programowania dynamicznego nie są rozłączne(dlatego nie możemy zastosować metody dziel i zwyciężaj), jednakże cechują się własnością optymalnej podstruktury.

Procedura wygląda następująco:
 * Podproblemy liczymy w kolejności od najmniej złozonych do najbardziej złożonych, ich kolejność nie powinna być cykliczna. Rozwiązania podproblemów zapisujemy.
 * Posiadając obliczone rozwiązania podproblemów, wykorzystujemy je do rozwiązania(zoptymalizowania) problemu.

Zdefiniowany uprzednio zbiór podproblemów wymaganych do rozwiązania problemu jest większy od zbioru rozwiązań podproblemów będących częścią optymalnego rozwiązania problemu, jednakże unikamy dzięki temu obliczania podobnych podproblemów, a co za tym idzie, złożoności wykładniczej problemu.

Przykładami algorytmów programowania dynamicznego są: CYK, Viterbiego, Earleya, Floyda-Warshalla, Needlemana-Wunscha.

**Output z konsoli:**

```
DATA10 OUTPUT:
10
53 7 7
84 5 91
46 5 216
83 1 52
93 4 514
38 1 413
1 2 748
5 7 673
68 6 922
65 4 694

6 9 2 5 4 7 1 3 8 10

opt: 766

--------------------------------------------------------------------------

DATA11 OUTPUT:
11
53 7 8
84 5 100
91 7 286
46 5 238
83 1 58
93 4 566
38 1 455
1 2 823
5 7 740
68 6 1014
65 4 764

6 9 11 2 5 4 7 1 3 8 10

opt: 1012

--------------------------------------------------------------------------

DATA12 OUTPUT:
12
53 7 9
84 5 109
91 7 312
46 5 260
5 7 390
83 1 63
93 4 617
38 1 496
1 2 898
5 7 807
68 6 1106
65 4 833

6 9 11 2 12 5 4 7 1 3 8 10

opt: 817

--------------------------------------------------------------------------

DATA13 OUTPUT:
13
53 7 9
84 5 118
91 7 338
46 5 282
5 7 422
83 1 68
93 4 669
38 1 537
1 2 973
5 7 874
68 6 1198
65 4 903
63 7 1276

6 9 11 2 12 5 4 7 1 3 8 10 13

opt: 697

--------------------------------------------------------------------------

DATA14 OUTPUT:
14
53 7 10
84 5 127
91 7 364
46 5 303
5 7 455
83 1 74
93 4 720
38 1 579
1 2 1048
5 7 942
68 6 1290
65 4 972
63 7 1374
37 3 1362

6 9 11 2 12 5 4 7 1 3 8 10 13 14

opt: 639

--------------------------------------------------------------------------

DATA15 OUTPUT:
15
53 7 11
84 5 136
91 7 390
46 5 325
5 7 487
83 1 79
93 4 771
38 1 620
1 2 1122
5 7 1009
68 6 1382
65 4 1041
63 7 1472
37 3 1460
72 7 968

6 9 11 2 12 5 4 7 1 3 8 10 13 14 15

opt: 582

--------------------------------------------------------------------------

DATA16 OUTPUT:
16
53 7 12
84 5 145
91 7 415
46 5 347
5 7 520
83 1 84
93 4 823
38 1 661
1 2 1197
5 7 1076
68 6 1474
65 4 1111
63 7 1570
37 3 1557
72 7 1032
8 6 1402

6 9 11 2 12 5 4 7 1 3 8 10 13 14 15 16

opt: 565

--------------------------------------------------------------------------

DATA17 OUTPUT:
17
53 7 12
84 5 154
91 7 441
46 5 368
5 7 552
83 1 90
93 4 874
38 1 703
1 2 1272
5 7 1143
68 6 1566
65 4 1180
63 7 1668
37 3 1654
72 7 1097
8 6 1489
27 4 1290

6 9 11 2 12 5 4 7 1 3 8 10 13 14 15 16 17

opt: 559

--------------------------------------------------------------------------

DATA18 OUTPUT:
18
53 7 13
84 5 163
91 7 467
46 5 390
5 7 585
48 3 490
83 1 95
93 4 926
38 1 744
1 2 1347
5 7 1211
68 6 1658
65 4 1250
63 7 1767
37 3 1751
72 7 1161
8 6 1577
27 4 1366

6 9 11 2 12 18 5 4 7 1 3 8 10 13 14 15 16 17

opt: 595

--------------------------------------------------------------------------

DATA19 OUTPUT:
19
53 7 14
84 5 173
91 7 493
46 5 412
5 7 617
48 3 517
83 1 100
38 1 785
93 4 977
36 2 915
1 2 1422
5 7 1278
68 6 1751
65 4 1319
63 7 1865
37 3 1849
72 7 1226
8 6 1665
27 4 1442

6 9 11 2 12 18 5 7 4 19 1 3 8 10 13 14 15 16 17

opt: 583

--------------------------------------------------------------------------

DATA20 OUTPUT:
20
89 9 119
53 7 15
84 5 182
91 7 519
46 5 433
5 7 650
48 3 544
83 1 105
93 4 1028
38 1 827
36 2 963
1 2 1496
5 7 1345
68 6 1843
65 4 1389
63 7 1963
37 3 1946
72 7 1290
8 6 1752
27 4 1518

20 6 9 11 2 12 18 5 4 7 19 1 3 8 10 13 14 15 16 17

opt: 1503

--------------------------------------------------------------------------
```
