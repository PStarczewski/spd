#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include "../RPQ/common.hpp"

struct singleEntry {
	int time;
	int weight;
	int finishTime;
	int index;

	singleEntry(std::vector<int> vec, int index) : time(vec[0]), weight(vec[1]), finishTime(vec[2]), index(index) {}

	void print() {
		std::cout << time << " " << weight << " " << finishTime << " " << std::endl;
	}
};

int sumTime(std::vector<singleEntry>* data) {
	int sum = 0;

	for (int i = 0; i < data->size(); ++i) {
		sum += (*data)[i].time;
	}

	return sum;
}

std::vector<singleEntry>* sortPD(std::vector<singleEntry>* tasks) {
	std::vector<singleEntry>* temporaryTasks = new std::vector<singleEntry>;

	int timeSum = sumTime(tasks);
	int size = tasks->size();

	for (int i = 0; i < size; ++i) {
		int cost = 0, cellIndex = 0, lastCostInt = 0;
		
		for (int j = 0; j < tasks->size(); ++j) {
			if (cost < ((*tasks)[j].time - (*tasks)[j].finishTime + timeSum) * (*tasks)[j].weight) {
				cost = ((*tasks)[j].time - (*tasks)[j].finishTime + timeSum) * (*tasks)[j].weight;
				cellIndex = j;
			}
		}

		temporaryTasks->push_back((*tasks)[cellIndex]);
		
		if (lastCostInt < cost) {
			std::swap((*temporaryTasks)[temporaryTasks->size() - 1], (*tasks)[cellIndex]);
		}
		lastCostInt = cost;

		std::vector<singleEntry>::iterator it = tasks->begin();
		std::advance(it, cellIndex);
		tasks->erase(it);
	}

	return temporaryTasks;
}

int main() {
	std::vector<singleEntry>* vec;
	std::vector<singleEntry>* vec2;

	for (int i = 10; i <= 20; ++i) {
		std::string name = "data" + std::to_string(i) + ".txt";
		vec = loadDataToVector<singleEntry>(name, 3);
		vec2 = sortPD(vec);
		WiTi::printDataset(i, vec2);
	}
}